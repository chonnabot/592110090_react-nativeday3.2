export default (state = [], action) => {
    switch(action.type){
        case 'LOGIN_NAME':
            return[...state, action.topic]
        case 'REMOVE_TODO':
            return state.filter((item, index) => index === action.index)
        default:
            return state
    }
}