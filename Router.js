import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import App1 from './App1'
import App2 from './App2'
import App3 from './App3'
import App4 from './App4'
import App5 from './App5'
import App6 from './App6'
import App7 from './App7'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/App1" component={App1} />
                    <Route exact path="/App2" component={App2} />
                    <Route exact path="/App3" component={App3} />
                    <Route exact path="/App4" component={App4} />
                    <Route exact path="/App5" component={App5} />
                    <Route exact path="/App6" component={App6} />
                    <Route exact path="/App7" component={App7} />
                    <Redirect to="/App1"/>
                </Switch>
            </NativeRouter>
        )
    }
}
export default Router