import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity, ScrollView } from 'react-native';

class App2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
            message: " ",
            username: '',
        };
    }
    // onShowModal = (message) => {
    //     this.setState({ isShow: true, message })
    // }
    // onHideModal = () => {
    //     this.setState({ isShow: false, })
    // }
    goToScreen3 = () => {
        return this.props.history.push('/App3')
    }
    goToScreen5 = () => {
        return this.props.history.push('/App5')
    }
    goToScreen6 = () => {
        return this.props.history.push('/App6')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerBox1}>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>

                    </View>
                    <View style={styles.headerBox2}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>List</Text>
                        </View>
                    </View>
                    <View style={styles.headerBox3}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>X</Text>
                        </View>
                    </View>
                </View>

                <ScrollView style={styles.body}>
                    <View style={styles.row}>
                            <View style={styles.box1}>
                                <TouchableOpacity>
                                    <Text style={styles.Text}
                                         onPress={this.goToScreen6}
                                    >Lorme1</Text>
                                </TouchableOpacity>
                            </View>
                        
                            <View style={styles.box2} >
                                    <Text style={styles.Text}
                                        onPress={this.goToScreen6}
                                    >Lorme2</Text>
                            </View>
                    </View>


                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                    onPress={this.goToScreen6}
                                >Lorme3</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme4</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme5</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme6</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme7</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme8</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme9</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2} >
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme10</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                     onPress={this.goToScreen6}
                                >Lorme11</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.Text}
                                   onPress={this.goToScreen6}
                                >Lorme12</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* <Modal
                        transparent={true}
                        visible={this.state.isShow}
                    >
                        <TouchableOpacity style={styles.modal}
                            onPress={() => { this.onHideModal() }}>
                            <Text style={styles.headerText}>{this.state.message}</Text>
                        </TouchableOpacity>
                    </Modal> */}
                    {/* </View> */}
                </ScrollView>

                <View style={styles.footer}>
                    <View style={styles.footerBox1}>
                        <Text style={styles.Text}>L</Text>
                    </View>
                    <View style={styles.footerBox2}>
                        <TouchableOpacity>
                            <Text style={styles.Text} onPress={this.goToScreen5}>Add</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.footerBox3}>
                        <Text style={styles.Text} onPress={this.goToScreen3}>P</Text>
                    </View>
                </View>

            </View>
        );                                                  //marginHorizontal: -8
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    Text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 0.1,
    },
    headerBox1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        alignItems: 'center', // เลื่อน ซ้ายไปขวา
    },
    headerBox2: {
        backgroundColor: '#00BFFF',
        flex: 3,
        margin: 2,
    },
    headerBox3: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',
        flex: 10,
        margin: 2,
        // justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        // alignItems: 'center', // เลื่อน ซ้ายไปขวา
        marginHorizontal: -1,
    },
    bodyScrollView: {
        backgroundColor: 'red',
        flexDirection: 'row',
        margin: 2,
        marginHorizontal: -1,
    },
    row: {
        backgroundColor: '#666',
        flexDirection: 'row',
        flex: 1,
    },
    box1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
        width: 200,
        height: 200,
    },
    box2: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
        width: 200,
        height: 200,
    },
    modal: {
        backgroundColor: "rgba(192,192,192,0.6)",
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 30,
    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 0.1,
        margin: 1,
    },
    footerBox1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        alignItems: 'center',
    },
    footerBox2: {
        backgroundColor: '#00BFFF',
        flex: 3,
        margin: 2,
        alignItems: 'center',
    },
    footerBox3: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    ham: {
        width: 35,
        height: 3.5,
        backgroundColor: 'white',
        margin: 2,
    },


});

export default App2


